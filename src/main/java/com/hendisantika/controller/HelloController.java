package com.hendisantika.controller;

import com.hendisantika.aspect.RequiresCaptcha;
import com.hendisantika.dto.HelloDTO;
import com.hendisantika.dto.HelloResponseDTO;
import com.hendisantika.dto.TokenResource;
import com.hendisantika.service.RecaptchaService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-recaptcha-aop
 * User: powercommerce
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 10/8/22
 * Time: 05:52
 * To change this template use File | Settings | File Templates.
 */
@RestController
@RequestMapping("/")
@RequiredArgsConstructor
public class HelloController {
    private final RecaptchaService recaptchaService;

    @PostMapping("hello")
    @RequiresCaptcha
    public HelloResponseDTO hello(@RequestBody HelloDTO helloDTO) {
        return new HelloResponseDTO("Hello, " + helloDTO.getName() + "!");
    }

    @PostMapping("/api/v1/saveScore")
    public String saveScore(@RequestBody TokenResource tokenResource) throws Exception {
        return recaptchaService.checkValidity(tokenResource.getToken());
    }
}
