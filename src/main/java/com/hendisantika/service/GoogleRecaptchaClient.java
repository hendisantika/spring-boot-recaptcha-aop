package com.hendisantika.service;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.hendisantika.model.CaptchaResponse;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-recaptcha-aop
 * User: powercommerce
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 10/8/22
 * Time: 06:09
 * To change this template use File | Settings | File Templates.
 */
@Service
@RequiredArgsConstructor
public class GoogleRecaptchaClient {
    private static final Logger logger = LoggerFactory.getLogger(GoogleRecaptchaClient.class);
    private final String mSpaceHost = "https://www.google.com/recaptcha/api/siteverify";
    private final RestTemplate restTemplate;

    public CaptchaResponse checkScore(String token) throws Exception {
        try {
            RestTemplate restTemplate = new RestTemplate();
            UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(mSpaceHost)
                    .queryParam("secret", "6LfqNpQgAAAAACXR6d-2DaOPADtr2y0UKvBDYlwX")
                    .queryParam("response", token);
            ResponseEntity exchange = restTemplate.exchange(builder.toUriString(), HttpMethod.POST, null, String.class);
            return new ObjectMapper().readValue((JsonParser) exchange.getBody(), CaptchaResponse.class);
        } catch (HttpClientErrorException | HttpServerErrorException e) {
            logger.error("Error response : State code: {}, response: {} ", e.getStatusCode(), e.getResponseBodyAsString());
            throw e;
        } catch (Exception err) {
            logger.error("Error: {} ", err.getMessage());
            throw new Exception("This service is not available at the moment!");
        }

    }
}
