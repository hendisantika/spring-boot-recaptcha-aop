package com.hendisantika.service;

import com.hendisantika.model.CaptchaResponse;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-recaptcha-aop
 * User: powercommerce
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 10/8/22
 * Time: 06:08
 * To change this template use File | Settings | File Templates.
 */
@Service
@RequiredArgsConstructor
public class RecaptchaService {
    @Autowired
    private GoogleRecaptchaClient googleRecaptchaClient;

    public String checkValidity(String token) throws Exception {
        if ("FIRST_TIME".equals(token)) {
            return "FIRST_TIME";
        } else {
            CaptchaResponse recaptchaResponse = googleRecaptchaClient.checkScore(token);
            if (!recaptchaResponse.getSuccess()) {
                if ("timeout-or-duplicate".equals(recaptchaResponse.getErrorCodes().get(0))) {
                    return "timeout-or-duplicate";
                } else {
                    return "Error: " + recaptchaResponse.getErrorCodes().get(0);
                }
            } else if (recaptchaResponse.getSuccess() && recaptchaResponse.getScore() <= 0.5) {
                return "Score: " + recaptchaResponse.getScore();
            } else if (recaptchaResponse.getSuccess() && recaptchaResponse.getScore() > 0.5) {
                return "Score: " + recaptchaResponse.getScore();
            }
        }
        return token;
    }
}
