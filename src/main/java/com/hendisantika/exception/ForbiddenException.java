package com.hendisantika.exception;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-recaptcha-aop
 * User: powercommerce
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 10/8/22
 * Time: 05:45
 * To change this template use File | Settings | File Templates.
 */
public class ForbiddenException extends RuntimeException {

    public ForbiddenException(String message) {
        super(message);
    }
}
